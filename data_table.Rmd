---
title: "data_table"
author: "Eva Hu"
date: "6/19/2020"
output: html_document
---

```{r, eval=FALSE}
#require(data.table)
input <- if (file.exists("flights14.csv")) {
   "flights14.csv"
} else {
  "https://raw.githubusercontent.com/Rdatatable/data.table/master/vignettes/flights14.csv"
}
flights <- fread(input)
flights
dim(flights)
```

```{r, eval=FALSE}
DT = data.table(
  ID = c("b","b","b","a","a","c"),
  a = 1:6,
  b = 7:12,
  c = 13:18
)
DT
class(DT$ID)
```

```{r}
getOption("datatable.print.nrows")
```

```{r,eval=FALSE}
ans <- flights[origin == "JFK" & month == 6L]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[1:2]
ans
```

```{r,eval=FALSE}
ans <- flights[order(origin, -dest)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[, arr_delay]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[, list(arr_delay)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[, .(arr_delay, dep_delay)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[, .(delay_arr = arr_delay, delay_dep = dep_delay)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[, sum( (arr_delay + dep_delay) < 0 )]
ans
```

```{r,eval=FALSE}
ans <- flights[origin == "JFK" & month == 6L,
               .(m_arr = mean(arr_delay), m_dep = mean(dep_delay))]
ans
```

```{r,eval=FALSE}
ans <- flights[origin == "JFK" & month == 6L, length(dest)]
ans
```

```{r,eval=FALSE}
ans <- flights[origin == "JFK" & month == 6L, .N]
ans
```

```{r,eval=FALSE}
ans <- flights[, c("arr_delay", "dep_delay")]
head(ans)
```

```{r,eval=FALSE}
select_cols = c("arr_delay", "dep_delay")
flights[ , ..select_cols]
```

```{r,eval=FALSE}
flights[ , select_cols, with = FALSE]
```

```{r,eval=FALSE}
DF = data.frame(x = c(1,1,1,2,2,3,3,3), y = 1:8)

## (1) normal way
DF[DF$x > 1, ] # data.frame needs that ',' as well

## (2) using with
DF[with(DF, x > 1), ]
```

```{r,eval=FALSE}
# returns all columns except arr_delay and dep_delay
ans_1 <- flights[, !c("arr_delay", "dep_delay")]
# or
ans_2 <- flights[, -c("arr_delay", "dep_delay")]

# returns year,month and day
ans_3 <- flights[, year:day]
# returns day, month and year
ans_4 <- flights[, day:year]
# returns all columns except year, month and day
ans_5 <- flights[, -(year:day)]
ans_6 <- flights[, !(year:day)]

ans_1
ans_2
ans_3
ans_4
ans_5
ans_6
```

```{r,eval=FALSE}
ans <- flights[, .(.N), by = .(origin)]
ans
```

```{r,eval=FALSE}
ans <- flights[, .(.N), by = "origin"]
ans
```

```{r,eval=FALSE}
ans <- flights[, .N, by = origin]
ans
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA", .N, by = origin]
ans
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA", .N, by = .(origin, dest)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA", .N, by = c("origin", "dest")]
ans
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA",
        .(mean(arr_delay), mean(dep_delay)),
        by = .(origin, dest, month)]
ans
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA",
        .(mean(arr_delay), mean(dep_delay)),
        keyby = .(origin, dest, month)]
ans
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA", .N, by = .(origin, dest)]
ans
```

```{r,eval=FALSE}
ans <- ans[order(origin, -dest)]
head(ans)
```

```{r,eval=FALSE}
ans <- flights[carrier == "AA", .N, by = .(origin, dest)][order(origin, -dest)]
head(ans, 10)
```

```{r,eval=FALSE}
ans <- flights[, .N, .(dep_delay>0, arr_delay>0)]
ans
```

```{r,eval=FALSE}
DT
DT[, print(.SD), by = ID]
# Empty data.table (0 rows and 1 cols): ID
```

```{r,eval=FALSE}
DT[, lapply(.SD, mean), by = ID]
```

```{r,eval=FALSE}
flights[carrier == "AA",                       ## Only on trips with carrier "AA"
        lapply(.SD, mean),                     ## compute the mean
        by = .(origin, dest, month),           ## for every 'origin,dest,month'
        .SDcols = c("arr_delay", "dep_delay")] ## for just those specified in .SDcols
```

```{r,eval=FALSE}
ans <- flights[, head(.SD, 2), by = month]
head(ans)
```

```{r,eval=FALSE}
DT[, .(val = c(a,b)), by = ID]
```

```{r,eval=FALSE}
DT[, .(val = list(c(a,b))), by = ID]
```

```{r,eval=FALSE}
## (1) look at the difference between
DT[, print(c(a,b)), by = ID]
# Empty data.table (0 rows and 1 cols): ID
```


```{r,eval=FALSE}
## (2) and
DT[, print(list(c(a,b))), by = ID]
# Empty data.table (0 rows and 1 cols): ID
```

